#!/usr/bin/env python
import sys
import os
import calendar
import urllib.request
import traceback

from xml.dom.minidom import parse
import xml.dom.minidom
from datetime import datetime

TASKFILE = 'Charm_Task_Definitions.xml'
URL_TASKLIST = "https://gitlab.com/youreperfectstudio/YourePerfectAdmin_Public/raw/master/Timesheets/Charm_Task_Definitions.xml"

def timediff(end, start):
	[dte, dts] = [datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ') for t in [end, start]]
	delta = dte - dts
	return delta.seconds

def format_interval(seconds):
	mins = seconds / 60
	hours = mins / 60
	mins = mins % 60

	result = '%dh' % hours
	if mins > 0:
		result = result + '%dm'%mins
	return result

def format_hours(seconds):
	mins = seconds / 60
	hours = mins / 60

	return '%dh' % hours
	
def process_event(event_node):
	start = event_node.getAttribute('start')
	end = event_node.getAttribute('end')
	taskid = event_node.getAttribute('taskid')
	interval = timediff(end, start)

	return {'start': start, 'end': end, 'taskid': taskid, 'interval' : interval}

def display_event(e):
	pass
	#print(e['taskid'], e['task']['title'], e['interval'], e['start'], e['end'], format_interval(e['interval']))

def parse_timesheet(filepath):
	result = {}
	DOMTree = xml.dom.minidom.parse(filepath)
	collection = DOMTree.documentElement

	# Parse Metadata
	
	metadata_node = collection.getElementsByTagName('metadata')[0]
	user_node = metadata_node.getElementsByTagName('username')[0]
	year_node = metadata_node.getElementsByTagName('year')[0]
	serial_node =  metadata_node.getElementsByTagName('serial-number')[0]
	semantics_attr = serial_node.getAttribute('semantics')

	user = ''
	year = year_node.childNodes[0].data
	month = serial_node.childNodes[0].data
	
	result['metadata'] = [user, year, month]
	
	# Parse effort events
	events = []

	effort_nodes = collection.getElementsByTagName('effort')
	for effort_node in effort_nodes:
		event_nodes = effort_node.getElementsByTagName('event')
		for event_node in event_nodes:
			event = process_event(event_node)
			events.append(event)

	result['effort'] = events
	return result

def parse_tasklist():
	taskdict = {}
	xmldata = urllib.request.urlopen(URL_TASKLIST).read()

	DOMTree = xml.dom.minidom.parseString(xmldata)
	collection = DOMTree.documentElement
	
	task_nodes = collection.getElementsByTagName('task')
	for task_node in task_nodes:
		taskid = task_node.getAttribute('taskid')
		parentid = task_node.getAttribute('parentid')
		title = task_node.childNodes[0].data
		taskdict[taskid] = {'taskid': taskid, 'parentid': parentid, 'title': title}
 
	return taskdict

def add_file(state, user, fileinfo):
	[userid, year, month] = fileinfo['metadata']
	year = int(year)
	month = int(month)
	ydict = {}
	mdict = {}
	userinfo = {'events': {}}

	if not year in state['years'].keys():
		state['years'][year] = ydict
	else:
		ydict = state['years'][year]

	if not month in ydict.keys():
		ydict[month] = mdict
	else:
		mdict = ydict[month]

	if not user in mdict.keys():
		mdict[user] = userinfo
	else:
		userinfo = mdict[user]

	for e in fileinfo['effort']:
		taskid = e['taskid']
		e['task'] = state['taskdict'][taskid]
		display_event(e)
		if not taskid in userinfo['events']:
			userinfo['events'][taskid] = e
		else:
			userinfo['events'][taskid]['interval'] = userinfo['events'][taskid]['interval'] + e['interval']

def display_state(state):
	ftotals = {}
	utotals = {}
	total	= 0
	for year in state['years'].keys():
		ydict = state['years'][year]
		print("%d,,,,,,,,,,"% year)
		for month in ydict.keys():
			print(",%s,,,,,,,,,"%calendar.month_name[month])
			mdict = ydict[month]
			mtotals = {}

			for user in mdict.keys():
				print(",,%s,,,,,,,,"%user)
				uinfo = mdict[user]
				if not user in utotals.keys():
					utotals[user] = 0

				for taskid in uinfo['events'].keys():
					event = uinfo['events'][taskid]
					print(',,,%s,%s,%s,,,,,' % (taskid,event['task']['title'], format_interval(event['interval'])))

					# Update totals
					utotals[user] = utotals[user] + event['interval']
					total = total + event['interval']
					
					if not taskid in ftotals.keys():
						ftotals[taskid] = 0
					ftotals[taskid] = ftotals[taskid] + event['interval']

					if not taskid in mtotals.keys():
						mtotals[taskid] = 0
					mtotals[taskid] = mtotals[taskid] + event['interval']

			print(",,%s Totals,,,,,,,,,"%calendar.month_name[month])
			for taskid in mtotals.keys():
				print(',,,%s,%s,%s,,,,,' % (taskid,state['taskdict'][taskid]['title'], format_interval(mtotals[taskid])))

	print("Forever Totals,,,,,,,,,,")
	for taskid in ftotals.keys():
		print(',,,%s,%s,%s,,,,,,' % (taskid,state['taskdict'][taskid]['title'], format_interval(ftotals[taskid])))

	print("Total time spent on all tasks ever:,%s,,,,,,,,,,,,," % format_hours(total))
	for user in utotals.keys():
		percent = 100.0 * utotals[user] / total
		print("%s's contribution:,%s,%0.1f %%,,,,,,,,,,,,,," % (user, format_hours(utotals[user]),percent))

def list_files(dirpath):
    return [os.path.join(dirpath, name) for name in os.listdir(dirpath)
            if not os.path.isdir(os.path.join(dirpath, name))]

def list_folders(dirpath):
    return [os.path.join(dirpath, name) for name in os.listdir(dirpath)
            if os.path.isdir(os.path.join(dirpath, name))]

def do_dir(state, username, dirpath):
	for file in list_files(dirpath):
		print("Processing %s ..."%file, file=sys.stderr)
		try:
			fileinfo = parse_timesheet(file)
			add_file(state, username, fileinfo)
		except:
			e = sys.exc_info()[0]
			print("	Skipping %s (Failed with error %s)" % (file,e), file=sys.stderr)
			print(traceback.format_exc(), file=sys.stderr)
				
def process_timesheets(rootpath):
	state = {'years': {}}
	print("Loading tasklist ...", file=sys.stderr)
	state['taskdict'] = parse_tasklist()
	for dirpath in list_folders(rootpath):
		do_dir(state, os.path.basename(dirpath), dirpath)

	
	return state
	#return [state, fileinfo['metadata']['userid'], fileinfo]	

if (len(sys.argv) < 2):
	print("Usage: %s <path to timesheet root folder>" % sys.argv[0], file=sys.stderr)
else:
	state = process_timesheets(sys.argv[1])
	display_state(state)


