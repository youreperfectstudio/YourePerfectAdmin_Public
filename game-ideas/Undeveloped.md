# Simple 1-5 ideas that are not further developed. 
They do not need to be good ideas. Many here are bad ideas or even jokes. That is ok.
If you develop an idea here it is polite to credit the person that wrote it just like
"Special thanks <person>" or Special Thanks Person@You're Perfect Studio or Special Thanks You're Perfect Studio
You are not expected to monetarily compensate anyone for ideas in this file. If your game blows up and
you make a million dollars it would polite to spend some of it on the idea originator's games/patreon/etc. as a thank you.

# game where you drive a scooter/moped around the streets of Houston
* From: Catherine
* Inspiration: Ramen on Wheels youtube chanel

# Repair manual game level
* From: Dr. Okra
* https://cdn.discordapp.com/attachments/496755420015362068/577403142849691649/haynes-workshop-manual-vw-type-2-1600cc-van-448-p.jpg
* ```would an image like that be a cool game level? I used to pore over these in the library when I was a youngun```